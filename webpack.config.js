const path = require("path");
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = {
	entry: "./src/main",
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "script.js"
	},
	resolve: {
		extensions: [".ts", ".js"],
		modules: [
			"node_modules",
			"./src"
		]
	},
	module: {
		rules: [
			{
				test: /\.ts|\.js$/,
				exclude: /node_modules/,
				use: "babel-loader"
			}
		]
	},
	plugins:[
		new CaseSensitivePathsPlugin()
	],
	// TODO: Create alternate configs
	mode: "development",
	devtool: "source-map",
	devServer: {
		contentBase: "./public",
		host: "0.0.0.0",
		port: 80,
		compress: true,
		hot: false,
		inline: false
	}
};
