import Box from "../Box";

export default interface Shape {
	readonly boundingBox: Box;
	draw2d(ctx: CanvasRenderingContext2D): void;
}
