import Box from "../Box";
import Shape from "./Shape";

export default class Rectangle implements Shape {
	constructor(
		public x: number,
		public y: number,
		public width: number,
		public height: number,
	) { }
	
	get boundingBox(): Box {
		return new Box(
			this.x,
			this.y,
			this.x + this.width,
			this.y + this.height,
		);
	}
	
	public draw2d(ctx: CanvasRenderingContext2D) {
		ctx.fillRect(this.x, this.y, this.width, this.height);
	}
}
