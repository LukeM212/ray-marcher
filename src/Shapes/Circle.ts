import Box from "../Box";
import Shape from "./Shape";

export default class Rectangle implements Shape {
	constructor(
		public x: number,
		public y: number,
		public radius: number,
	) { }
	
	get boundingBox(): Box {
		return new Box(
			this.x - this.radius,
			this.y - this.radius,
			this.x + this.radius,
			this.y + this.radius,
		);
	}
	
	public draw2d(ctx: CanvasRenderingContext2D) {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
		ctx.fill();
	}
}
