import QuadTree from "./QuadTree";
import Shape from "./Shapes/Shape";
import Rectangle from "./Shapes/Rectangle";
import Circle from "./Shapes/Circle";

import {randBetween} from "./Helpers/random"

export default class Map {
	private content: QuadTree<Shape> = new QuadTree<Shape>(s => s.boundingBox, 0, 0, this.width, this.height);
	
	constructor(
		public width: number,
		public height: number,
	) {
		for (let i = 0; i < 10; i++) {
			if (Math.random() < 0.5) {
				const width = 10 / Math.random();
				const height = 10 / Math.random();
				const x = randBetween(-width, this.width);
				const y = randBetween(-height, this.height);
				this.content.add(new Rectangle(x, y, width, height));
			} else {
				const radius = 5 / Math.random();
				const x = randBetween(-radius, this.width + radius);
				const y = randBetween(-radius, this.height + radius);
				this.content.add(new Circle(x, y, radius));
			}
		}
	}
	
	public draw2d(ctx: CanvasRenderingContext2D) {
		for (const shape of this.content.all()) {
			shape.draw2d(ctx);
		}
	}
}
