import Box from "./Box";
import Lazy from "./Helpers/Lazy";
import {genFilter} from "./Helpers/Generator";

export default class QuadTree<T> {
	private cx: number = (this.minx + this.maxx) / 2;
	private cy: number = (this.miny + this.maxy) / 2;
	
	private tl: Lazy<QuadTree<T>> = new Lazy(() => new QuadTree(this.bounds, this.minx, this.miny, this.cx, this.cy));
	private tr: Lazy<QuadTree<T>> = new Lazy(() => new QuadTree(this.bounds, this.cx, this.miny, this.maxx, this.cy));
	private bl: Lazy<QuadTree<T>> = new Lazy(() => new QuadTree(this.bounds, this.minx, this.cy, this.cx, this.maxy));
	private br: Lazy<QuadTree<T>> = new Lazy(() => new QuadTree(this.bounds, this.cx, this.cy, this.maxx, this.maxy));
	
	private elems: T[] = [];
	
	constructor(
		private bounds: (t: T) => Box,
		private minx: number,
		private miny: number,
		private maxx: number,
		private maxy: number,
	) { }
	
	public add(t: T) {
		const tBox = this.bounds(t);
		
		if (tBox.maxy <= this.cy) {
			if (tBox.maxx <= this.cx) this.tl.get.add(t);
			else if (tBox.minx >= this.cx) this.tr.get.add(t);
			else this.elems.push(t);
		} else if (tBox.miny >= this.cy) {
			if (tBox.maxx <= this.cx) this.bl.get.add(t);
			else if (tBox.minx >= this.cx) this.br.get.add(t);
			else this.elems.push(t);
		} else this.elems.push(t);
	}
	
	public *all(): Generator<T, void, undefined> {
		yield* this.elems;
		yield* this.tl.val?.all() ?? [];
		yield* this.tr.val?.all() ?? [];
		yield* this.bl.val?.all() ?? [];
		yield* this.br.val?.all() ?? [];
	}
	
	public *in(box: Box): Generator<T, void, undefined> {
		yield* genFilter(t => this.bounds(t).intersects(box), this.elems);
		yield* this.tl.val?.in(box) ?? [];
		yield* this.tr.val?.in(box) ?? [];
		yield* this.bl.val?.in(box) ?? [];
		yield* this.br.val?.in(box) ?? [];
	}
}
