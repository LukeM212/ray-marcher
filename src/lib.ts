export function make<K extends keyof HTMLElementTagNameMap>(name: K, props?: { [prop: string]: any }) {
	const elem = document.createElement(name);
	return Object.assign(elem, props);
}