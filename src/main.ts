import "regenerator-runtime/runtime";

import Game from "./Game";

const game = new Game(
	1280, 720,
	512, 512,
);

document.body.appendChild(game.canvas);
