export default class Box {
	constructor(
		public minx: number,
		public miny: number,
		public maxx: number,
		public maxy: number,
	) { }
	
	intersects(box: Box): boolean {
		return (
			this.minx < box.maxx &&
			this.maxx > box.minx &&
			this.miny < box.maxy &&
			this.maxy > box.miny
		);
	}
}
