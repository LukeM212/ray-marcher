export function* genMap<T, U>(f: (t: T) => U, g: Iterable<T>): Generator<U, void, undefined> {
	for (const x of g) yield f(x);
}

export function* genFilter<T>(f: (t: T) => boolean, g: Iterable<T>): Generator<T, void, undefined> {
	for (const x of g) if (f(x)) yield x;
}
