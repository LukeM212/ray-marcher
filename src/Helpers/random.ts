export function randBetween(from: number, to: number) {
	return Math.random() * (to - from) + from;
}