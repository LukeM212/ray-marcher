export default class Lazy<T> {
	public val?: T;
	
	constructor(private run: () => T) { }
	
	public get get() { return this.val || (this.val = this.run()); }
}
