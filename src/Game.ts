import Map from "./Map";

import {make} from "./lib";

export default class Game {
	public canvas: HTMLCanvasElement = make('canvas', { width: this.mapWidth, height: this.mapHeight});
	public ctx: CanvasRenderingContext2D = this.canvas.getContext('2d')!;
	
	public map: Map = new Map(this.mapWidth, this.mapHeight);
	
	constructor(
		public screenWidth: number,
		public screenHeight: number,
		public mapWidth: number,
		public mapHeight: number,
	) {
		this.map.draw2d(this.ctx);
	}
}
